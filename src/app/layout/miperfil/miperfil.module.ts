import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MiperfilRoutingModule } from './miperfil-routing.module';
import { PageHeaderModule } from './../../shared';
import { MiperfilComponent } from './miperfil.component';


@NgModule({
  imports: [
    CommonModule,
    PageHeaderModule,
    MiperfilRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule.forRoot(),
  ],
  declarations: [MiperfilComponent]
})
export class MiperfilModule { }
