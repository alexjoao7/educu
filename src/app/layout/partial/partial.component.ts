import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

export interface Notas {
  nombre: string;
  promedio_final: number;
  quimestre_uno: number;
  qu_parcial_uno: number;
  qu_parcial_dos: number;
  qu_parcial_tres: number;
  qu_examen: number;
  quimestre_dos: number;
  qd_parcial_uno: number;
  qd_parcial_dos: number;
  qd_parcial_tres: number;
  qd_examen: number;
}

@Component({
  selector: 'app-partial',
  templateUrl: './partial.component.html',
  styleUrls: ['./partial.component.scss'],
  animations: [routerTransition()]
})
export class PartialComponent implements OnInit {

  public notas: Array<Notas> = [];
  public searchText:string;

  constructor() { }

  ngOnInit() {
    console.log("Prueba");
    this.notas=[
      {
        nombre: "Lenguaje",
        promedio_final: 5.23,
        quimestre_uno: 6.89,
        qu_parcial_uno: 3.89,
        qu_parcial_dos: 9.95,
        qu_parcial_tres: 5.58,
        qu_examen: 6.15,
        quimestre_dos: 7.77,
        qd_parcial_uno: 9.75,
        qd_parcial_dos: 7,
        qd_parcial_tres: 6.65,
        qd_examen: 5.75
      },
      {
        nombre: "Matemáticas",
        promedio_final: 7.23,
        quimestre_uno: 6.89,
        qu_parcial_uno: 3.89,
        qu_parcial_dos: 9.95,
        qu_parcial_tres: 5.58,
        qu_examen: 9.15,
        quimestre_dos: 7.77,
        qd_parcial_uno: 9.75,
        qd_parcial_dos: 9,
        qd_parcial_tres: 6.65,
        qd_examen: 5.75
      },
      {
        nombre: "Inglés",
        promedio_final: 7.23,
        quimestre_uno: 6.89,
        qu_parcial_uno: 3.89,
        qu_parcial_dos: 9.95,
        qu_parcial_tres: 5.58,
        qu_examen: 9.15,
        quimestre_dos: 7.77,
        qd_parcial_uno: 9.75,
        qd_parcial_dos: 9,
        qd_parcial_tres: 6.65,
        qd_examen: 5.75
      }
    ]

    console.log(this.notas);
  }

}
