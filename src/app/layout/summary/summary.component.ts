import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';

export interface Promedio {
  curso: String;
  paralelo: String;
  tutor: String;
  promedio: String;
  faltas: String;
  comportamiento: String;

}

export interface Materias {
  materia: String;
  docente: String;
  calificacion: String;
  faltas: String;
  comportamiento: String;
}

export interface Parciales {
  id: String;
  descripcion: String;
  estado: String;
}

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss'],
  animations: [routerTransition()]
})
export class SummaryComponent implements OnInit {

  public promedio: Promedio;
  public materias: Materias[] = [];
  public parciales: Parciales[] = [];
  public seleparcial: Parciales;

  public url: String = "";
  constructor() { }

  ngOnInit() {
    this.url = "assets/images/usuario.png";
    this.getPromedioStudent();
    this.getMateriasStudent();
    this.getParciales();
  }

  /**
   * Consulta los promedios del estudiante
   */
  getPromedioStudent() {
    this.promedio =
      {
        curso: "Noveno de básica",
        paralelo: "C",
        tutor: "Lic. Luis Bustamante",
        promedio: "8.95",
        faltas: "8",
        comportamiento: "A"
      }

  }

  /**
   * Listado de materias
   */
  getMateriasStudent() {
    this.materias = [
      {
        materia: "Física",
        docente: "Paula Ochoa",
        calificacion: "8.96",
        faltas: "1",
        comportamiento: "A",
      },
      {
        materia: "Química",
        docente: "Laura Jácome",
        calificacion: "5.24",
        faltas: "1",
        comportamiento: "B",
      },
      {
        materia: "Literatura",
        docente: "Alexander Romero",
        calificacion: "7.54",
        faltas: "1",
        comportamiento: "A",
      },
      {
        materia: "Literatura",
        docente: "Alexander Romero",
        calificacion: "7.54",
        faltas: "1",
        comportamiento: "A",
      },
      {
        materia: "Literatura",
        docente: "Alexander Romero",
        calificacion: "7.54",
        faltas: "1",
        comportamiento: "A",
      },
      {
        materia: "Literatura",
        docente: "Alexander Romero",
        calificacion: "7.54",
        faltas: "1",
        comportamiento: "A",
      },
      {
        materia: "Literatura",
        docente: "Alexander Romero",
        calificacion: "7.54",
        faltas: "1",
        comportamiento: "A",
      }]
  }

  /**
   * Funcion que permite obtener la descripcion del año lectivo
   */
  getParciales() {
    this.parciales = [
      { id: "RA", descripcion: "Resumen del año", estado: "1" },
      { id: "PA1", descripcion: "Parcial 1", estado: "1" },
      { id: "PA2", descripcion: "Parcial 2", estado: "1" },
      { id: "PA3", descripcion: "Parcial 3", estado: "0" },
      { id: "QM1", descripcion: "Quimestre 1", estado: "0" },
      { id: "PA4", descripcion: "Parcial 1", estado: "0" },
      { id: "PA5", descripcion: "Parcial 2", estado: "0" },
      { id: "PA6", descripcion: "Parcial 3", estado: "0" },
      { id: "QM2", descripcion: "Quimestre 2", estado: "0" },
    ];

    this.seleparcial = this.parciales[0];
  }

  /**
   * Funcion que permite obtener la informacion del parcial seleccionado
   * @param parcial 
   */
  getInformacion(parcial: Parciales) {
    this.seleparcial = parcial;
  }

}
